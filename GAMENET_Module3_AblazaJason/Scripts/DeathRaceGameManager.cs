using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class DeathRaceGameManager : MonoBehaviourPunCallbacks
{
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;

    public static DeathRaceGameManager instance = null;

    public TMP_Text timeText;
    public TMP_Text killfeedText;
    public TMP_Text winnerText;

    public int NumberOfPlayersLeft;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    /*
    public void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    */

    private void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                GameObject spawnedVehicle = PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, startingPositions[actorNumber - 1].rotation);
                
                spawnedVehicle.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                spawnedVehicle.GetComponent<VehicleMovement>().speed /= 2f;

                //PhotonNetwork.NetworkingClient.EventReceived += spawnedVehicle.GetComponent<Health>().OnEvent;
            }

            StartCoroutine(CO_DelayInitialize());
        }
    }

    /*
    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.PlayerDiedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            string nickNameOfKiller = (string)data[1];
            int viewId = (int)data[2];

            Debug.Log("OnEvent received by DeathRaceGameManager");
            NumberOfPlayersLeft--;

            if (NumberOfPlayersLeft <= 1)
            {
                Debug.Log($"{nickNameOfDeadPlayer}, {nickNameOfKiller}, viewID: {viewId}");
                //photonView.RPC("OnPlayerWinDR", RpcTarget.AllBuffered, nickNameOfKiller, viewId);
                //OnPlayerWinDR(nickNameOfKiller, viewId);
            }
        }
    }
    */

    private IEnumerator CO_DelayInitialize()
    {
        yield return new WaitForSeconds(2);
        NumberOfPlayersLeft = PhotonNetwork.CurrentRoom.PlayerCount;
    }
}
