using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera _camera;
    public GameObject _playerUiPrefab;

    // Start is called before the first frame update
    void Start()
    {
        this._camera = transform.Find("Camera").GetComponent<Camera>();
        /*
        if (photonView.IsMine)
        {
            GameObject playerUi = Instantiate(_playerUiPrefab);
        }
        */

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            _camera.enabled = photonView.IsMine;
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;

            //rotate to compensate for track start
            this.gameObject.transform.Rotate(new Vector3(0, -90, 0));

            //turn off weapon prefabs used in Death Race mode
            GameObject weapon = gameObject.GetComponentInChildren<Weapon>().gameObject;

            if (weapon != null )
            {
                weapon.SetActive(false);
            }
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            _camera.enabled = photonView.IsMine;
            GetComponentInChildren<Weapon>().enabled = photonView.IsMine;
        }
    }
}
