using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(Rigidbody))]

public class Projectile : MonoBehaviour
{
    private Rigidbody _rb;

    public float _speed;
    private int _damage;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.useGravity = false;
        _rb.AddForce(transform.up * -_speed, ForceMode.Impulse);

        Destroy(this.gameObject, 4.0f);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<PhotonView>() == null)
            return;

        if (!other.gameObject.GetComponent<PhotonView>().IsMine)
        {
            if (other.gameObject.GetComponent<Health>() != null)
            {
                Debug.Log(other.gameObject.name);
                //other.gameObject.GetComponent<Health>().TakeDamage(_damage);
                
                other.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, _damage);
            }
            Destroy(this.gameObject);
        }
    }

    public void SetDamageValue(int damage)
    {
        _damage = damage;
    }

    public void SetProjectileSpeedValue(float speed)
    {
        _speed = speed;
    }
}
