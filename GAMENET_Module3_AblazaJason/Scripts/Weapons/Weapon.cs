using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Weapon : MonoBehaviourPunCallbacks
{
    protected bool _canShoot;
    [SerializeField] protected int _damage;
    [SerializeField] protected float _fireRateDelay;

    virtual protected void Start()
    {
        _canShoot = true;
    }

    protected void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Shoot();
        }
    }

    virtual protected void Shoot()
    {
        if (_canShoot)
        {
            StartCoroutine(CO_StartFireRateDelay());
        }
    }
    protected IEnumerator CO_StartFireRateDelay()
    {
        _canShoot = false;
        yield return new WaitForSeconds(_fireRateDelay);
        _canShoot = true;
    }
}
