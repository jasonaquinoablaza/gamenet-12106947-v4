using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject _cameraManager;
    public Camera _camera;

    void Start()
    {
        //_cameraManager = GameObject.Find("Camera Holder");
        _camera = _cameraManager.transform.GetChild(0).GetComponentInChildren<Camera>();

        if (photonView.IsMine)
            _cameraManager.GetComponent<CameraManager>().target = this.gameObject.transform;
        else
        {
            _cameraManager.GetComponent<CameraManager>().enabled = false;
        }

        _camera.enabled = photonView.IsMine;

        GetComponent<CharacterControls>().enabled = photonView.IsMine;

        //_camera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("oc"))
        {
            
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("jc"))
        {

        }

        /*
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            _camera.enabled = photonView.IsMine;
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;

            //rotate to compensate for track start
            this.gameObject.transform.Rotate(new Vector3(0, -90, 0));

            //turn off weapon prefabs used in Death Race mode
            GameObject weapon = gameObject.GetComponentInChildren<Weapon>().gameObject;

            if (weapon != null )
            {
                weapon.SetActive(false);
            }
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            _camera.enabled = photonView.IsMine;
            GetComponentInChildren<Weapon>().enabled = photonView.IsMine;
        }
        */
    }
}
