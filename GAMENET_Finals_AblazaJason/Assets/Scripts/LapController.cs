using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }

    private int finishOrder = 0;

    private void OnEnable()
    {
        //bind as a listener
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        //unbind as a listener
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            //retrieve data sent by the event
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinishedPlayer + " - Position: " + finishOrder);

            GameObject orderUiText = ObstacleCourseManager.instance.finisherTextUi[finishOrder - 1];
            orderUiText.SetActive(true);

            if (photonView.IsMine)
            {
                //if this GO is the player
                if (viewId == photonView.ViewID)
                {
                    orderUiText.GetComponent<TMP_Text>().text = $"{finishOrder} - {nickNameOfFinishedPlayer} (YOU)";
                    orderUiText.GetComponent<TMP_Text>().color = Color.red;
                }
                else
                {
                    orderUiText.GetComponent<TMP_Text>().text = $"{finishOrder} - {nickNameOfFinishedPlayer}";
                }
            }
        }
    }

    private void Start()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("oc"))
        {
            foreach (GameObject go in ObstacleCourseManager.instance.lapTriggers)
            {
                lapTriggers.Add(go);
            }
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("jc"))
        {
            /*
            foreach (GameObject go in ObstacleCourseManager.instance.lapTriggers)
            {
                lapTriggers.Add(go);
            }
            */
        }
        //this.enabled = false;
    }

    private void OnTriggerEnter(Collider col)
    {
        if(lapTriggers.Contains(col.gameObject) && photonView.IsMine)
        {
            int indexOfTrigger = lapTriggers.IndexOf(col.gameObject);

            lapTriggers[indexOfTrigger].SetActive(false);


            if (col.gameObject.tag == "FinishTrigger")
            {
                GameFinish();
            }
        }
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>()._camera.transform.parent = null;
        GetComponent<CharacterControls>().enabled = false;

        finishOrder++;

        Debug.Log("Game Finish");
        
        string nickName = photonView.Owner.NickName;

        int viewId = photonView.ViewID;

        //Event Data to be passed in the event
        object[] data = new object[] { nickName, finishOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        //Send Event
        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    }
}
