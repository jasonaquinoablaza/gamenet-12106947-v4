using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using ExitGames.Client.Photon;
using Photon.Realtime;

public enum RaiseEventsCode
{
    PlayerDiedEventCode = 1
}

public class Health : MonoBehaviourPunCallbacks, IOnEventCallback
{
    private bool _isAlive;
    public int NumberOfPlayersLeft;
    public GameObject killfeedTextUI;
    public Slider _slider;

    #region Health Getters and Setters
    public int CurrentHealth
    {
        get => _currentHealth;
    }

    public int MaxHealth
    {
        get => _maxHealth;
        set => _maxHealth = value;
    }

    [SerializeField] private int _currentHealth;
    [SerializeField] private int _maxHealth;

    private void Awake()
    {
        _isAlive = true;
        _currentHealth = _maxHealth;
        StartCoroutine(CO_DelayInitialize());
        _slider = GetComponentInChildren<Slider>();
        _slider.value = (float)_currentHealth / (float)_maxHealth;
    }

    public void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    [PunRPC]
    public void TakeDamage(int damageTaken, PhotonMessageInfo info)
    {
        Debug.Log(this.gameObject.name + " took damage of " + damageTaken);
        _currentHealth -= damageTaken;

        _slider.value = (float)_currentHealth / (float)_maxHealth;

        if (_currentHealth <= 0 && _isAlive) 
        {
            _isAlive = false;
            _currentHealth = 0;
            //if (photonView.IsMine)
                DoDeath(info);
        }
    }
    #endregion

    #region Event Related

    public void OnEvent(EventData photonEvent)
    {
        //On a Player's Death
        if (photonEvent.Code == (byte)RaiseEventsCode.PlayerDiedEventCode)
        {
            Debug.Log("OnEvent received by Health.cs, inside if photonEventCode");

            //retrieve data sent by the event
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            string nickNameOfKiller = (string)data[1];
            int viewId = (int)data[2];

            Debug.Log($"Dead: {nickNameOfDeadPlayer} | Killer: {nickNameOfKiller}, viewId: {viewId}");

            //UI related code here
            GameObject killfeedUiText = DeathRaceGameManager.instance.killfeedText.gameObject;
            killfeedUiText.SetActive(true);

            string text = "";
            if (photonView.IsMine)
            {
                //killfeedTextUI.GetComponent<TMP_Text>().text
                if (photonView.Owner.NickName == nickNameOfDeadPlayer)
                {
                    text = $"{nickNameOfKiller} eliminated {nickNameOfDeadPlayer} (YOU)";
                }
                else if (photonView.Owner.NickName == nickNameOfKiller)
                {
                    text = $"{nickNameOfKiller} (YOU) eliminated {nickNameOfDeadPlayer}";
                }
                else
                {
                    text = $"{nickNameOfKiller} eliminated {nickNameOfDeadPlayer}";
                }

                StartCoroutine(CO_DisplayKillfeed(text));
            }
        }
    }

    public void DoDeath(PhotonMessageInfo info)
    {
        Debug.Log("Attempting DoDeath()");
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<PlayerSetup>()._camera.transform.parent = null;
        GetComponent<Weapon>().enabled = false;
        GetComponentInChildren<Weapon>().enabled = false;

        if (photonView.IsMine)
        {
            string nickName = photonView.Owner.NickName;
            string killerName = info.Sender.NickName;
            int viewId = photonView.ViewID;

            DeathRaceGameManager.instance.NumberOfPlayersLeft--;

            if (DeathRaceGameManager.instance.NumberOfPlayersLeft <= 1)
            {
                photonView.RPC("OnPlayerWinDR", RpcTarget.AllBuffered, killerName, viewId);
            }

            //Event Data to be passed in the event
            object[] data = new object[] { nickName, killerName, viewId };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            //Send Event
            PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.PlayerDiedEventCode, data, raiseEventOptions, sendOptions);
        }
    }
    #endregion

    #region RPC
    [PunRPC]
    public void OnPlayerWinDR(string winnerName, int viewId)
    {
        TMP_Text winnerText = DeathRaceGameManager.instance.winnerText;
        winnerText.text = $"{winnerName} WINS!";
        
    }
    #endregion

    #region Coroutines
    private IEnumerator CO_DelayInitialize()
    {
        yield return new WaitForSeconds(1);
        NumberOfPlayersLeft = PhotonNetwork.CurrentRoom.PlayerCount;
        killfeedTextUI = GameObject.Find("Killfeed Text");
    }

    private float killfeedTime = 0;
    [SerializeField] private float killfeedDisplayDuration = 10.0f;

    private IEnumerator CO_DisplayKillfeed(string text)
    {
        killfeedTime = killfeedDisplayDuration;

        while (killfeedTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            killfeedTime -= 1;

            if (killfeedTextUI != null)
            {
                killfeedTextUI.GetComponent<TMP_Text>().text = text;
            }
        }

        killfeedTextUI.GetComponent<TMP_Text>().text = "";
    }
    #endregion
}
