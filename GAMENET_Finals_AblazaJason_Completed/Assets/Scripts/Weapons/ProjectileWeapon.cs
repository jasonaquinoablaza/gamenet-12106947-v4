using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UIElements;

public class ProjectileWeapon : Weapon
{
    [Header("Projectile Prefab")]
    [SerializeField] private GameObject _projectilePrefab;
    [SerializeField] private GameObject _projectileSpawnPositionGO;
    [SerializeField] private float _projectileSpeed;

    override protected void Shoot()
    {
        if (_canShoot)
        {
            photonView.RPC("SpawnProjectile", RpcTarget.AllBuffered);
            base.Shoot();
        }
    }

    [PunRPC]
    public void SpawnProjectile()
    {
        GameObject _spawnedProjectile = Instantiate(_projectilePrefab, _projectileSpawnPositionGO.transform.position, _projectileSpawnPositionGO.transform.rotation);
        _spawnedProjectile.GetComponent<Projectile>().SetDamageValue(_damage);
        _spawnedProjectile.GetComponent<Projectile>().SetProjectileSpeedValue(_projectileSpeed);
    }
}
