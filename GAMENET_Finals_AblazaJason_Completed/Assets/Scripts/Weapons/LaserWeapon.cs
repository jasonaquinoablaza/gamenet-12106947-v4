using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LaserWeapon : Weapon
{
    public Camera _camera;

    override protected void Start()
    {
        _camera = transform.parent.GetComponentInChildren<Camera>();
        base.Start();
    }

    override protected void Shoot()
    {
        if (_canShoot)
        {
            RaycastHit hit;
            Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

            if (Physics.Raycast(ray, out hit, 300))
            {
                Debug.Log($"Laser weapon hit {hit.collider.gameObject.name}");
                //For when another player gets hit and prevent player from hitting self
                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, _damage);
                }
            }

            base.Shoot();
        }
    }
}