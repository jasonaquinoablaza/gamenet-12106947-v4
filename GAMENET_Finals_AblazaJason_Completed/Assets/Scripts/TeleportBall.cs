using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBall : ProjectileBase
{
    protected override void Start()
    {
        base.Start();
        _rb.AddForce(transform.up * (_speed * 0.365f), ForceMode.Impulse);
        _rb.useGravity = true;
    }
}
