using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    [Header("Primary Fire")]
    public bool _canShoot;
    [SerializeField] private float _fireDelay;
    [SerializeField] private float _projectileSpeed;

    [Header("Secondary Fire")]
    [SerializeField] private float _teleportBallSpeed;
    [SerializeField] private float _teleportBallDuration;
    [SerializeField] private GameObject _teleportBallReference;
    [SerializeField] private bool _hasTeleportBall;
    public bool _isTeleportBallOut;

    //Reminder: Edit collission properties to prevent projectiles clashing with each other
    [Header("References")]
    [SerializeField] private List<GameObject> _projectilePrefabs;
    [SerializeField] private Transform _projectileSpawnPoint;
    public CharacterControls _characterControls;

    private void Start()
    {
        //should be disabled via countdown manager
        //_canShoot = false;

        _canShoot = true;
        _characterControls = GetComponent<CharacterControls>();
        _hasTeleportBall = true;
    }

    private void Update()
    {
        if (!photonView.IsMine)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (_canShoot)
            {
                PrimaryFire();
            }          
        }

        if (Input.GetMouseButtonDown(1))
        {
           
            if (_isTeleportBallOut)
            {
                //Teleport to Ball early if the ball is already out
                TeleportToTeleportBall();
            }

            if (_hasTeleportBall && !_isTeleportBallOut)
            {
                ShootTeleportBall();
            }
        }
    }

    [PunRPC]
    private void ShootProjectile(int prefabIndex, float speed, bool isTransformReferenceNeeded)
    {
        //Using PunRPC instead of PhotonView.Instantiate because latter needs PhotonView Component on instantiated GameObject

        //spawn projectile
        GameObject projectileGO = Instantiate(_projectilePrefabs[prefabIndex], _projectileSpawnPoint.position, _projectileSpawnPoint.rotation);      

        //give properties
        ProjectileBase projectileScript = projectileGO.GetComponent<ProjectileBase>();
        if (projectileScript != null)
        {
            projectileScript._shootingScript = this;
            projectileScript._speed = speed;

            if (isTransformReferenceNeeded)
            {
                _teleportBallReference = projectileGO;
            }

            //imply it is primary projectile. enum would be better?
            else
            {
                projectileScript._ownerCharacterControls = _characterControls;
            }
        }
    }

    #region Primary Projectile

    private void PrimaryFire()
    {
        //ShootProjectile(_projectilePrefab, _projectileSpeed, false);

        photonView.RPC("ShootProjectile", RpcTarget.AllBuffered, 0, _projectileSpeed, false);

        StartCoroutine(CO_ShootPrimaryProjectile());
    }

    private IEnumerator CO_ShootPrimaryProjectile()
    {
        _canShoot = false;
        yield return new WaitForSeconds(_fireDelay);
        _canShoot = true;
    }

    public void DestroyTarget(GameObject targetGO)
    {
        PhotonView.Destroy(targetGO);
    }
    #endregion

    #region Teleport Ball
    private void ShootTeleportBall()
    {
        //ShootProjectile(_teleportBallPrefab, _teleportBallSpeed, true);

        photonView.RPC("ShootProjectile", RpcTarget.AllBuffered, 1, _teleportBallSpeed, true);

        StartCoroutine(CO_ShootTeleportBall());
    }

    [PunRPC]
    public void OnTeleportBallPickUp()
    {
        if (!_isTeleportBallOut)
        {
            _hasTeleportBall = true;
        }
    }

    public void TeleportToTeleportBall()
    {
        _isTeleportBallOut = false;

        if (_teleportBallReference == null)
        {
            return;
        }

        //Get TP Ball Pos
        Transform tpPos = _teleportBallReference.transform;

        //Destroy TP ball
        Destroy(_teleportBallReference);
        _teleportBallReference = null;

        //Teleport to TP ball
        transform.position = tpPos.position;
    }

    private IEnumerator CO_ShootTeleportBall()
    {
        _isTeleportBallOut = true;
        _hasTeleportBall = false;
        yield return new WaitForSeconds(_teleportBallDuration);

        if (_isTeleportBallOut)
        {
            _isTeleportBallOut = false;
            PhotonView.Destroy(_teleportBallReference.gameObject);
            _teleportBallReference = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp_TeleportBall"))
        {
            photonView.RPC("OnTeleportBallPickUp", RpcTarget.AllBuffered);
            PhotonView.Destroy(other.gameObject);
        }
    }
    #endregion
}
