using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(Rigidbody))]
public class ProjectileBase : MonoBehaviour
{
    public Shooting _shootingScript;
    protected Rigidbody _rb;

    public float _lifetime;
    public float _speed;

    public CharacterControls _ownerCharacterControls;

    virtual protected void Start()
    {
        _rb = GetComponent<Rigidbody>();

        _rb.AddForce(transform.forward * _speed, ForceMode.Impulse);
    }
}
