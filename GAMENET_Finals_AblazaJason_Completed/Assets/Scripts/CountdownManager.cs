using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public TMP_Text timerText;

    public float timeToStartRace = 3.0f;

    private void Start()
    {
        timerText = ObstacleCourseManager.instance.timeText;

        GetComponent<Shooting>().enabled = false;
    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (timeToStartRace > 0)
            {
                timeToStartRace -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartRace);
            }
            else if (timeToStartRace < 0)
            {
                photonView.RPC("StartRace", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void SetTime(float time)
    {
        if (timerText == null) return;

        if (time > 0)
        {
            timerText.text = time.ToString("F1");
        }
        else
        {
            timerText.text = "";
        }
    }

    [PunRPC]
    public void StartRace()
    {
        GetComponent<CharacterControls>().isControlEnabled = true;
        GetComponent<Shooting>().enabled = PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("jc");
        this.enabled = false;
    }
}
