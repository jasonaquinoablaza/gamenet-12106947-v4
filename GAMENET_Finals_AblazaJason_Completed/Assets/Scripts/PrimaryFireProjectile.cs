using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(Rigidbody))]

public class PrimaryFireProjectile : ProjectileBase
{

    protected override void Start()
    {
        base.Start();

        _rb.useGravity = false;
        //_rb.AddForce(transform.forward * _speed, ForceMode.Impulse);

        transform.Rotate(new Vector3(90, 0, 0));

        Destroy(gameObject, _lifetime);
    }

    private void OnCollisionEnter(Collision other)
    {
        //Do not destroy object if it hits the player that owns the projectile
        if (other.gameObject.GetComponent<PhotonView>() != null)
        {
            if (!other.gameObject.GetComponent<PhotonView>().IsMine)
            {
                Destroy(gameObject);
            }
        }

        else
        {
            Destroy(gameObject);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SpeedTarget"))
        {
            Debug.Log("Projectile hit SpeedTarget");
            
            _shootingScript.DestroyTarget(other.gameObject);

            OnTriggerWithSpeedTarget();
        }
    }

    /*
    [PunRPC]
    private void OnCollisionWithSpeedTarget(GameObject speedTargetGO)
    {
        _ownerCharacterControls.IncreaseSpeedTemporarily();
        Destroy(speedTargetGO);
        Destroy(gameObject);
    }
    */

    public void OnTriggerWithSpeedTarget()
    {
        _ownerCharacterControls.IncreaseSpeedTemporarily();
        Destroy(gameObject);
    }
}
