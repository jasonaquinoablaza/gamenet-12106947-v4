using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public bool _isRotating;
    public float _rotationSpeed;

    protected void Update()
    {
        if (_isRotating)
        {
            transform.Rotate(0f, _rotationSpeed * Time.deltaTime / 0.01f, 0f, Space.Self);
        }    
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject, 0.1f);
        }
    }
}
