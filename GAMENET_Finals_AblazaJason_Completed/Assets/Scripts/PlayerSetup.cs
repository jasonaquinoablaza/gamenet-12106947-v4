using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject _cameraManager;
    public Camera _camera;

    void Start()
    {
        //_cameraManager = GameObject.Find("Camera Holder");
        _camera = _cameraManager.transform.GetChild(0).GetComponentInChildren<Camera>();

        if (photonView.IsMine)
            _cameraManager.GetComponent<CameraManager>().target = this.gameObject.transform;
        else
        {
            _cameraManager.GetComponent<CameraManager>().enabled = false;
        }

        _camera.enabled = photonView.IsMine;

        GetComponent<CharacterControls>().enabled = photonView.IsMine;
    }
}
